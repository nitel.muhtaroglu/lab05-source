package cs102;

public class Main {
    public static void main(String[] args) {
        Student studentOne = new Student("Ali Demir", new Mentor("Hasan Sozer"));
        Student studentTwo = new Student("Ayse Yilmaz", new Mentor("Guney Guven Yapici"));

        int age = 20;
        double gpa = 2.6;
        studentOne.setAge(age);
        studentOne.setDepartment("Computer Science");
        studentOne.setGPA(gpa);

        age = 21;
        studentTwo.setAge(age);
        studentTwo.setDepartment("Mechanical Engineering");
        studentTwo.setGPA(3.1);

        System.out.println(studentOne);
        System.out.println(studentTwo);
    }
}
