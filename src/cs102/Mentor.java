package cs102;

public class Mentor {
    private String name;
    private String title;

    public Mentor(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
