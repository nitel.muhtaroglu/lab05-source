package cs102;

public class Student {
    String name;
    int age;
    String department;
    double gpa;
    Mentor mentor;

    public Student(String name, Mentor mentor) {
        this.name = name;
        this.mentor = mentor;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setGPA(double gpa) {
        this.gpa = gpa;
    }

    public String toString() {
        String s = "Name: " + this.name + "\n"
                + "Age: " + this.age + "\n"
                + "Department: " + this.department + "\n"
                + "GPA: " + this.gpa + "\n"
                + "Mentor: " + this.mentor;
        return s;
    }
}
